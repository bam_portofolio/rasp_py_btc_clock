#!/usr/bin/env python3

#BTXv1.2 - Bam

import tm1637
import datetime as dt
from binance.client import Client

#Name each display and define pins
#Top Mini = tmi, CLK = GPIO10, DI0 = GPIO11
tno = tm1637.TM1637(23,24,tm1637.BRIGHT_TYPICAL)
bno = tm1637.TM1637(17,22,tm1637.BRIGHT_TYPICAL)
tmi = tm1637.TM1637(10,11,tm1637.BRIGHT_TYPICAL)
bmi = tm1637.TM1637(2,3,tm1637.BRIGHT_TYPICAL)

disps = [tno, bno, tmi, bmi]
brights = [4, 3, 4, 4]
values = []

#Get all ticker prices from Binance
client = Client("", "")
all_tickers = client.get_all_tickers()

#Top Normal display with BTC_USD
b_ticker = [d for d in all_tickers if d['symbol'] == 'BTCUSDT'][0]
b_price = float(b_ticker['price'])
b_value = str(round(b_price)).rjust(6, '0')
values.append(b_value)

#Bot Normal display with VTHO_USD
c_ticker = [d for d in all_tickers if d['symbol'] == 'VTHOUSDT'][0]
c_price = float(c_ticker['price'])
c_price = c_price * 1e6
c_value = str(round(c_price)).rjust(6, '0')
values.append(c_value)

#Top Mini display with VET_USDT
d_ticker = [d for d in all_tickers if d['symbol'] == 'VETUSDT'][0]
d_price = float(d_ticker['price'])
d_price = d_price * 1e5
d_value = str(round(d_price)).rjust(6, '0')
values.append(d_value)

#Bot Mini display with Clock (DDHHMM)
z_value = dt.datetime.now().strftime('%d%H%M')
values.append(z_value)

#For the other displays/tickers
# for coin, factor in zip(coins, factors):
    # ticker = [d for d in all_tickers if d['symbol'] == coin][0]
    # price_f = float(ticker['price'])
    # price = price_f * b_price * factor
    # value = str(round(price)).rjust(6, '0')
    # values.append(value)

#Reorder numbers (because weird chinese reverse backwards displays)
#And send output to displays
for disp, bright, data in zip(disps, brights, values):
        order = data[2], data[1], data[0], data[5], data[4], data[3]
        price = list(map(int, order))
        disp.Clear()
        disp.SetBrightnes(bright)
        disp.Show(price)

