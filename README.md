### RaspberryPi Python Bitcoin Clock/Ticker
Rapsberry Pi project with four regularly updated 6-digit LED displays.  
Three displays show bitcoin/crypto prices. One shows the date and time.   
Runs on a Pi Zero W, but should run on any model with the GPiO pins.  

#### Details
Displays: [RobotDyn 6-Digit LED Display](https://www.tinytronics.nl/shop/nl/verlichting/led-segment-display/robotdyn-segmenten-display-module-6-karakters-decimalen-groen-tm1637)  
Library: [TM1637 Tiny Display](https://github.com/jasonacox/TM1637TinyDisplay)  
Crypto prices: [Python Binance](https://github.com/sammchardy/python-binance)  

#### Use
Create a python3 virtual environment (venv) and `pip install -r requirements.txt`  
Run every x minutes through cron, `crontab -e`, and add the line from *cron* file.  

#### Image
![PiZeroW with four btc tickers](rasp_py_btc.jpg "PiZeroW BTC tickers")
